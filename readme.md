# Presentation

Ce depot contient tout les exemples que j'ai pu presenter lors des veilles que j'ai faites

## Liens vers les slides

### Bash

16/10/23

C'etait la premiere presentation, faut que je recupere ce que j'avais fait je le mettrai par la soon TM

### Regex

14/11/23

[slide sur les regex](https://docs.google.com/presentation/d/1bZcKe62C-WU5EmQZ5APhmtFbrUOqX9aRDiYPwCEI8o0/edit?usp=sharing)

### automate

30/11/23

[slide sur les automates](https://docs.google.com/presentation/d/1C8HMvkKEhq8cGTN1ex9FPqLQDna8u_DiOW4maItpGFY/edit?usp=sharing)

### git workflow

05/12/23

[slide sur git](https://docs.google.com/presentation/d/134mzzd1aiVehH3lu4wfT9EkrItp3OySovSwg_yafVwY/edit?usp=sharing)

[fichier avec les procedures importantes](./git.md)

### fonnctions recursive

03/01/24

[slide sur les fonctions recursive](https://docs.google.com/presentation/d/1HxzH34e9JWg88v2aDVC10HQfz2OvwcfSR8VLuhj09sE/edit?usp=sharing)

[exemple file](./recursive_func.ipynb)
### Decorator

25/01/24

[slide sur les decorators](https://docs.google.com/presentation/d/1FG6ntvU57SPiUynm-n-5oHg-NRRaShAyrvv40L-FyLY/edit?usp=sharing)

[exemple file](./decorator.ipynb)

### Partition

31/01/24

Il n'y a pas de slide !

[exemple file](./sql_window_function.md)
