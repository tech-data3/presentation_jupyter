# How to work with git (easy version)

## Vos meilleurs copains

Quand vous lancez une commande dans le terminal, lisez ce qu'elle vous retourne.

### Dans le doute

Toujours faire ```git status```

## Work on feature

```mermaid
flowchart TD
    chose_task(choisir une tache sur le trello)
    branch_name_selection(choisir un nom de branch et l'ecrire sur la carte trello correspondant a la tache)
    checkout_master(git checkout master)
    pull(git pull --all)
    branch_creation(git checkout -b nouvelle_feature)
    work(realisation du travail en fonction de la tache et rien d'autre)
    commit(git add -A\ngit commit -m '')
    pull_push(git pull\n git push)
    tache_finished{la tache est termine ?}

    debut --> chose_task --> branch_name_selection --> checkout_master --> pull --> branch_creation --> work --> commit --> pull_push --> tache_finished --yes--> chose_task
    tache_finished -- no -->work
```

## Merge

```mermaid
flowchart TD
    pull(git pull --all)
    branch(git branch --all)
    checkout(git checkout branche_a_merger)
    checkout_dump(git checkout -b branche_a_merger_dump)
    cleaning(nettoyer l'arbre de travail)
    add_commit(git add -A\n git commit -m 'cleaning branch_name')
    checkout_master(git checkout master)
    merge(git merge branche_a_merger_dump)
    conflict{il ya des conflits}
    conflict_handling(gerer les conflits)
    add_commit_push_master(git add -A\n git commit -m 'merge branche')
    push(git push)
    pull --> branch
    branch --> checkout
    checkout --> checkout_dump
    checkout_dump --> cleaning
    cleaning --> add_commit
    add_commit --> checkout_master
    checkout_master --> merge
    merge --> conflict
    conflict -- yes --> conflict_handling --> add_commit_push_master --> push
    conflict -- no --> push
```

## project management

its the first attempt not so good frenglish and step missing just a try there

```mermaid
flowchart TD
    pbd(product breackdown diagram)
    observe_difficulties{do some feature seems to be hard}
    activity_diagram_diff(make an activity diagram of what seems difficult)
    mcd_mld(data models\nwhat we want and what we have)
    decide_task_from_diagram(decide whiche task you have to do to finish the projet)
    decide_what_is_important(decide what is important to do and rank task with the order of importance)
    pbd --> observe_difficulties -- yes --> activity_diagram_diff-->mcd_mld
    observe_difficulties -- no -->mcd_mld
    mcd_mld --> decide_task_from_diagram --> decide_what_is_important
    determine_names(determine names of things that will be expose to everybody\nfunc name\nbranch naing convention)
    populate_trello(populate trello with tasks in the right order)
    decide_what_is_important --> determine_names -->populate_trello
    chose_task(choisir une tache sur le trello)
    branch_name_selection(choisir un nom de branch et l'ecrire sur la carte trello correspondant a la tache)
    checkout_master(git checkout master)
    pull(git pull --all)
    branch_creation(git checkout -b nouvelle_feature)
    work(realisation du travail en fonction de la tache et rien d'autre)
    commit(git add -A\ngit commit -m '')
    pull_push(git pull\n git push)
    tache_finished{la tache est termine ?}
    task_empty{is there any task left ?}
    populate_trello --> chose_task --> branch_name_selection --> checkout_master --> pull --> branch_creation --> work --> commit --> pull_push
    tache_finished --yes--> task_empty --yes-->chose_task
    task_empty -- no -->pbd
    tache_finished -- no -->work
    start --> pbd
```

### Diagram to do 

Remember this is an incremental process !

```mermaid
flowchart TD
mld_mcd(
    mld
    mcd
    class
)
difficult_part{
    is there any difficult part to handle ?
}
    pbd --> mld_mcd --> perth --> difficult_part 
    difficult_part -- yes --> activity --> sequence
    difficult_part -- no --> sequence --> maquete --> dev
```
