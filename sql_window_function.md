# Window Functions

## What is it ?

### window function

is a function on a window.

### window

a set of row on table or result set.

defined by OVER()

### result set

the result of a select

## sample dataset

For this example, we will use the dataset of the world exercises

## Window function

syntax :

```sql
function(expression|column) OVER(
	[ PARTITION BY expr_list optional]
    [ ORDER BY order_list optional]
)
```

### functions list :

aggregation function : ```SUM(), MAX(), COUNT()```... and so on

rank window function : ```RANK(), DENSE_RANK(), ROW_NUMBER()``` that allow to rank, or "index" row

value function : ```LAG(), LEAD(), FIRST_VALUE()``` that allow to get near row value

## Window function OVER everything

```sql
SELECT 
    DENSE_RANK() OVER(ORDER BY population DESC) AS `rank`,       
    code,     
    name, 
    100*population/SUM(population) OVER() AS `percentage of total population` 
FROM country
LIMIT 10;

```

| rank | code | name               | percentage of total population |
|:-----|:-----|:-------------------|-------------------------------:|
|    1 | CHN  | China              |                        21.0168 |
|    2 | IND  | India              |                        16.6755 |
|    3 | USA  | United States      |                         4.5792 |
|    4 | IDN  | Indonesia          |                         3.4893 |
|    5 | BRA  | Brazil             |                         2.7985 |
|    6 | PAK  | Pakistan           |                         2.5743 |
|    7 | RUS  | Russian Federation |                         2.4172 |
|    8 | BGD  | Bangladesh         |                         2.1247 |
|    9 | JPN  | Japan              |                         2.0845 |
|   10 | NGA  | Nigeria            |                         1.8344 |

In fact its cool but we can achieve the same things using sub request (not sure about ranking)

```sql
SELECT  
    code,          
    name,     
    100*population/(SELECT SUM(population) FROM country) AS `percentage of total population` 
FROM country 
ORDER BY `percentage of total population` DESC 
LIMIT 10;

```

| code | name               | percentage of total population |
|:-----|:-------------------|-----------------:|
| CHN  | China              |          21.0168 |
| IND  | India              |          16.6755 |
| USA  | United States      |           4.5792 |
| IDN  | Indonesia          |           3.4893 |
| BRA  | Brazil             |           2.7985 |
| PAK  | Pakistan           |           2.5743 |
| RUS  | Russian Federation |           2.4172 |
| BGD  | Bangladesh         |           2.1247 |
| JPN  | Japan              |           2.0845 |
| NGA  | Nigeria            |           1.8344 |

## Window function OVER PARTITION BY AND ORDER BY

The PARTITION BY divides the result set into different partitions/windows. 

The PARTITION BY clause by a column then the result-set will be divided into different windows of the value of that columns. 

In fact, to popularize, a partition by can be seen as a group by but you keep each line separate.

Remember when we have to find the most spoken official language of each country its easy now.

```sql
SELECT 
    countrycode,          
    language,          
    percentage,          
    ROW_NUMBER() OVER(PARTITION BY countrycode ORDER BY percentage DESC) AS rk      
FROM countrylanguage 
WHERE isofficial = 'T';

```

| countrycode | language         | percentage | rk |
|:------------|:-----------------|-----------:|---:|
| ABW         | Dutch            |        5.3 |  1 |
| AFG         | Pashto           |       52.4 |  1 |
| AFG         | Dari             |       32.1 |  2 |
| AIA         | English          |        0.0 |  1 |
| ALB         | Albaniana        |       97.9 |  1 |
| AND         | Catalan          |       32.3 |  1 |
| ANT         | Papiamento       |       86.2 |  1 |
| ANT         | Dutch            |        0.0 |  2 |
| ARE         | Arabic           |       42.0 |  1 |
| ARG         | Spanish          |       96.8 |  1 |
| ARM         | Armenian         |       93.4 |  1 |
| ASM         | Samoan           |       90.6 |  1 |
| ASM         | English          |        3.1 |  2 |
| ATG         | English          |        0.0 |  1 |
| AUS         | English          |       81.2 |  1 |
| AUT         | German           |       92.0 |  1 |
| AZE         | Azerbaijani      |       89.0 |  1 |
| BDI         | Kirundi          |       98.1 |  1 |
| BDI         | French           |        0.0 |  2 |
| BEL         | Dutch            |       59.2 |  1 |
| BEL         | French           |       32.6 |  2 |
| BEL         | German           |        1.0 |  3 |
| BGD         | Bengali          |       97.7 |  1 |

As you can see the row number function has been apply to each partition (look at bel and bdi) separatly.

```sql
SELECT 
    countrycode, 
    language, 
    percentage  
FROM     
    (SELECT 
        countrycode, 
        language,
        percentage, 
        ROW_NUMBER() OVER(PARTITION BY countrycode ORDER BY percentage DESC) AS rk 
    FROM countrylanguage
    WHERE isofficial = 'T') AS subr 
 AND subr.rk = 1;

```

if you want to use the result of the window function in the where clause you have to do it in a subrequest since the window function is the last thing that return a result (after where). so the request has to end to get the result.

to get a prettyier result i made a harder request, to give sense to data.

this request represent the most spoken official language for the 10 most populated country.

```sql
SELECT 
    subr.countrycode,
    c.name,
    subr.language AS `most spoken official language in the country`
FROM
    (SELECT
        countrycode,
        language,
        isofficial,
        percentage,
        ROW_NUMBER() OVER(PARTITION BY countrycode ORDER BY percentage DESC) AS rk
    FROM countrylanguage
    WHERE isofficial = "T") AS subr
JOIN
    country AS c ON c.code = subr.countrycode
WHERE rk = 1
ORDER BY c.population DESC
LIMIT 10;

```

| countrycode | name               | most spoken official language in the country |
|:------------|:-------------------|:---------------------------------------------|
| CHN         | China              | Chinese                                      |
| IND         | India              | Hindi                                        |
| USA         | United States      | English                                      |
| IDN         | Indonesia          | Javanese                                     |
| BRA         | Brazil             | Portuguese                                   |
| PAK         | Pakistan           | Punjabi                                      |
| RUS         | Russian Federation | Russian                                      |
| BGD         | Bangladesh         | Bengali                                      |
| JPN         | Japan              | Japanese                                     |
| NGA         | Nigeria            | Joruba                                       |

## Quick reference on window function

### rank dense_rank()

rank and dense_rank does basically the same rank results in the window by the order you choose if 2 line have the same value they have the same rank.

the difference between the two function is that if 2 line have the same rank (same value in comparaison) rank will keep gap 
1
1
3

where dense rank will output
1
1
2

### row_number() 

seems self explanatory

### lag()

lag is used to return the offset row before the current row within a window. By default it returns the previous row before the current row

### lead()

same as lag but with the next row

### first_value()

the first value of the window
